const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const mysql = require('mysql'); 
const { redirect } = require('express/lib/response');
/* ------------------------------------------------------ */
let app = express();
/* ------------------------------------------------------ */
app.use(bodyParser.json()); //!สำหรับ Parse json
app.use(morgan('dev'));
app.use(cors());
// use

// ดูเพิ่ม
app.use(bodyParser.urlencoded({extended:true})) //! แปลงข้อมูลที่ส่งจากฟอร์ม
app.use(express.static('public'));
//! ส่งข้อมูลไปหน้าบ้านโดย ejs 
app.set('views', __dirname+'/public/html');// ระบุตำแหน่งของไฟล์ html
app.engine('html', require('ejs').renderFile);


/* ------------------------------------------------------ */


var userLogin = {}

/* ------------------ connect database ------------------ */
const con = mysql.createConnection({
  host:'localhost',
  database:'std_probation',
  user:'root',
  password:''
});

con.connect((err)=>{
  if(err){
    console.log(err.message)
    return;
  }

  console.log("Connected Database")
});
/* ------------------------------------------------------ */

var userLogin= {}

/* --------------------- login.html --------------------- */
app.get('/',function(req,res,next){
  res.sendFile(__dirname+"/"+"public/"+"html/"+"FN-01_UI-01.html");
})
/* ------------------------------------------------------ */



/* --------------------- login.html --------------------- */
app.post('/login',function(req,res,next){
  const email  = req.body.email; //ดึงค่าจากตัวแปร email ใน forms
  const password = req.body.password;
  console.log(email , password)
  // ! ค้นหา user ที่มี id และ password ได้ array
  con.query("SELECT mst_lecture.* FROM `mst_security` INNER JOIN mst_lecture ON mst_security.id_employee = mst_lecture.id_employee WHERE user = ? && password = ?;",[email,password],function(err,data){

    if(err){
      console.log('Error ! for select data');
      return;
    }

    // console.log(data)

    if(data.length==0){
      console.log('length == 0')
      res.sendFile(__dirname+"/"+"public"+"/"+"html"+"/"+"FN-01_UI-01_none.html")
      return;
    }
    

    // ! assign ค่าให้ user 
    userLogin=data[0] 

    //? เพิ่มข้อมูล  trn login 
    con.query("INSERT INTO `trn_login` (`id_login`, `datetime_login`, `id_employee`) VALUES (NULL, current_timestamp(), ?)",[userLogin.id_employee]);

    //! เปลื่ยนหน้าเข้า main 
    res.redirect('/main')
  })
})
/* ------------------------------------------------------ */


/* ------------------------ main ------------------------ */
app.get('/main',function(req,res,next){
  res.sendFile(__dirname+'/public/'+'html/'+'FN-01-UI-02.html')
})
/* ------------------------------------------------------ */














/* ------------------- รอรับ port 8081 ------------------ */
app.listen(8081,function(){
})